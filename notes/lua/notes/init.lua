
local M = {}
local fzf_lua = require'fzf-lua'



--[[ Core Features ]]--



-- %#% Create new note in tmux window
M.new_note = function()

  local nvim_server = vim.api.nvim_exec2('echo v:servername', {output = true})["output"]
  local id = vim.fn.trim(vim.fn.system("date +%Y%m%d%H%M%S"))
  vim.cmd(':e ~/notes/notes/' .. id .. '.md')
  local tmux_command = 'tmux new-window -a -n new-note $HOME/.scripts/notes.sh -n ' .. id .. ' ' .. nvim_server
  vim.fn.system(tmux_command)

end




-- %#% Update title of existing note
M.update_title = function()

  local nvim_server = vim.api.nvim_exec2('echo v:servername', {output = true})["output"]
  local path = vim.api.nvim_buf_get_name(0)
  local id = string.match(path, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d')
  local tmux_command = 'tmux new-window -a -n new-note $HOME/.scripts/notes.sh -T ' .. id .. ' ' .. nvim_server
  vim.fn.system(tmux_command)

end


-- %#% Create a new note and insert link at cursor position
M.new_note_and_link = function()

  local nvim_server = vim.api.nvim_exec2('echo v:servername', {output = true})["output"]
  local id = vim.fn.trim(vim.fn.system("date +%Y%m%d%H%M%S"))
  local tmux_command = 'tmux new-window -a -n new-note $HOME/.scripts/notes.sh -N ' .. id .. ' ' .. nvim_server
  vim.fn.system(tmux_command)

end



-- %#% Filter notes on titles
M.title_search = function (init, opts)

   if (init > 0) then
    vim.fn.system("$HOME/.scripts/notes.sh -R")
   end

  opts = opts or {}
  opts.prompt           = "Titles> "
  opts.cwd              = "~/notes/notes"
  opts.previewer        = "fzf-native"

  opts.fzf_opts = {
    ["--cycle"] = '',
    ["--multi"] = '',
    ["--tac"] = '',
    ["--with-nth"] = '2',
    ["--delimiter"] = '~',
    -- ["--preview"] = vim.fn.shellescape("echo {1} | xargs -I id bat --style=plain --color=always " .. os.getenv("HOME") .. "/notes/notes/id.md"),
    ["--preview"] = "echo {1} | xargs -I id bat --style=plain --color=always $HOME/notes/notes/id.md",
    -- ["--preview"] = vim.fn.shellescape("rg -NI {} -- $HOME/notes/notes/.list-of-notes.txt | head -n1 | cut -f1 -d'~' | xargs -I id bat --style=plain --color=always /home/hugo/notes/notes/id.md"),
  }

  opts.keymap = {
    fzf = {
      ["ctrl-x"] = 'clear-query',
      ["alt-r"]  = "execute-silent($HOME/.scripts/notes.sh -R)+kill-line+reload(cat ~/notes/notes/.list-of-notes.txt)",
  }}

  opts.actions = {
    ["default"] = function (selected)
      local t = {}
     for _, f in ipairs(selected) do
        table.insert(t, f)
      end
      local output = table.concat(t, ' ')
      local f_list = vim.fn.trim(vim.fn.system('/home/hugo/.scripts/notes.sh -P "' .. output .. '" | tr "\n" " "'))
      local first_selected = vim.fn.trim(vim.fn.system("echo " .. f_list .. " | cut -f1 -d ' '"))
      -- -- Add list of files to arglist and edit first selected
      vim.api.nvim_command(':$argadd ' .. f_list)
      vim.api.nvim_command(":buffer " .. first_selected)
    end,
    ['alt-enter'] = { function(selected)
      local t = {}
      for _, f in ipairs(selected) do
        table.insert(t, f)
      end
      local output = table.concat(t, ' ')
      local f_list = vim.fn.trim(vim.fn.system('/home/hugo/.scripts/notes.sh -P "' .. output .. '" | tr "\n" " "'))
      -- Add list of files to arglist and edit first selected
      vim.api.nvim_command(':$argadd ' .. f_list)
    end,
      fzf_lua.actions.resume
    },
    ['alt-f'] = function(selected)
      local id =''
      for _, f in ipairs(selected) do
        id = string.match(f, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d')
      end
      local nvim_server = vim.api.nvim_exec2('echo v:servername', {output = true})["output"]
      vim.fn.system('$HOME/.scripts/notes.sh -L ' .. id .. ' ' .. nvim_server)
    end,
    ['alt-k'] = { function() M.tag_field_search(0) end },
    ['alt-l'] = { function() M.tag_list_search() end },
  }

  return fzf_lua.fzf_exec('cat $HOME/notes/notes/.list-of-notes.txt', opts)

end



-- %#% Filter notes based on YAML tags
M.tag_field_search = function (init, opts)

   if (init > 0) then
    vim.fn.system("$HOME/.scripts/notes.sh -R")
   end

  opts = opts or {}
  opts.prompt           = "Tags> "
  opts.cwd              = "~/notes/notes"
  opts.previewer        = "fzf-native"

  opts.fzf_opts = {
    ["--disabled"] = '',
    ["--cycle"] = '',
    ["--multi"] = '',
    ["--tac"] = '',
    ["--with-nth"] = '2',
    ["--delimiter"] = '~',
    ["--preview"] = "echo {1} | xargs -I id bat --style=plain --color=always " .. os.getenv("HOME") .. "'/notes/notes/id.md'",
  }

  opts.keymap = {
    fzf = {
      ["change"]  = "reload(sleep 0.05 ; rg -IN -e '~tags.*{q}' ~/notes/notes/.list-of-notes.txt | cut -f2 -d';' || true)",
      ["enter"]  = "select-all+execute-silent($HOME/.scripts/notes.sh -F {+})+execute-silent(~/.scripts/notes.sh -r)+deselect-all+beginning-of-line",
      ["ctrl-x"] = 'clear-query',
      ["alt-r"]  = "execute-silent($HOME/.scripts/notes.sh -R)+kill-line+reload(cat ~/notes/notes/.list-of-notes.txt)",
      ["alt-x"]  = "execute-silent(tmux new-window -a -n new-note $HOME/.scripts/notes.sh -X {+})+kill-line",
      -- NB: do not use ":" in reload(), execute(), ...
  }}

  opts.actions = {
    ["default"] = { function () end },
    ["alt-m"] = function (selected)
      local t = {}
      for _, f in ipairs(selected) do
        table.insert(t, f)
      end
      local output = table.concat(t, ' ')
      local f_list = vim.fn.trim(vim.fn.system('/home/hugo/.scripts/notes.sh -P "' .. output .. '" | tr "\n" " "'))
      local first_selected = vim.fn.trim(vim.fn.system("echo " .. f_list .. " | cut -f1 -d ' '"))
      -- -- Add list of files to arglist and edit first selected
      vim.api.nvim_command(':$argadd ' .. f_list)
      vim.api.nvim_command(":buffer " .. first_selected)
    end,
    ['alt-enter'] = { function(selected)
      local t = {}
      for _, f in ipairs(selected) do
        table.insert(t, f)
      end
      local output = table.concat(t, ' ')
      local f_list = vim.fn.trim(vim.fn.system('/home/hugo/.scripts/notes.sh -P "' .. output .. '" | tr "\n" " "'))
      -- Add list of files to arglist and edit first selected
      vim.api.nvim_command(':$argadd ' .. f_list)
    end,
      fzf_lua.actions.resume
    },
    ['alt-f'] = function(selected)
      local id =''
      for _, f in ipairs(selected) do
        id = string.match(f, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d')
      end
      local nvim_server = vim.api.nvim_exec2('echo v:servername', {output = true})["output"]
      vim.fn.system('$HOME/.scripts/notes.sh -L ' .. id .. ' ' .. nvim_server)
    end,
    ['alt-j'] = { function() M.title_search(0) end },
    ['alt-l'] = function() M.tag_list_search() end,
  }

  return fzf_lua.fzf_exec('cat $HOME/notes/notes/.list-of-notes.txt', opts)

end



-- %#% Menu with notes that links to current notes
M.list_backlinks = function (opts)

  local path = vim.api.nvim_buf_get_name(0)
  local id = string.match(path, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d')
  local fp = vim.fn.trim(vim.fn.system('/home/hugo/.scripts/notes.sh -B ' .. id))

  opts = opts or {}
  opts.prompt           = "Titles> "
  opts.cwd              = "~/notes/notes"

  opts.fzf_opts = {
    ["--cycle"] = '',
    ["--with-nth"] = '2',
    ["--delimiter"] = '~',
    ["--preview"] = "echo {1} | xargs -I id bat --style=plain --color=always id",
  }

  opts.winopts = {
    preview = {
      layout = "vertical",
      vertical = "up:70%", wrap = "wrap"
    },
    width = 0.8,
  }

   opts.actions = {
       ['default'] = function(selected)
         local path = string.match(selected[1], '%d+')
         vim.cmd('e ' .. '$HOME/notes/notes/' .. path .. '.md')
      end
   }

  return fzf_lua.fzf_exec('rg -NH --no-heading "^title:" -- ' .. fp .. ' | sed -e "s/:title:/ ~/"', opts)

end



-- %#% Select among unique tags in YAML field
M.tag_list_search = function (opts)

  opts = opts or {}
  opts.prompt           = "Tags> "
  opts.cwd              = "~/notes/notes"

  opts.fzf_opts = {
    ["--cycle"] = '',
    ["--multi"] = '',
    ["--tac"] = '',
  }

  opts.keymap = {
    fzf = {
      ["ctrl-x"] = 'clear-query',
      ["ctrl-q"] = 'replace-query',
  }}

  opts.actions = {
    ["default"] = { function (selected)
      local t = {}
      for _, f in ipairs(selected) do
        table.insert(t, f)
      end
      local tags = table.concat(t, ' ')
      vim.fn.system('/home/hugo/.scripts/notes.sh -f "' .. tags .. '"')
      M.title_search(0)
    end},
    ['alt-k'] = { function() M.tag_field_search(0) end },
  }


  return fzf_lua.fzf_exec("$HOME/.scripts/notes.sh -l", opts)

end



-- %#% Live query of infile tags (format: _|%tag::subtag|...|_)
M.infile_tags = function ()

  local fp = vim.api.nvim_buf_get_name(0)

  -- Fzf interface to select among all unique tags within current buffer
  fzf_lua.fzf_exec("rg -NI '_\\|%.*\\|_' -- " .. fp .. "  | perl -ne 'print \"$1\n\" while /(%.*?)\\|/g;' | sort -u ", {
    keymap = {
      fzf = {
        -- Reload with only "root" tags
        ["alt-j"] = "reload(rg -NI  '_\\|%.*\\|_' -- " .. fp .. " | perl -ne 'print \"$1\n\" while /(%.*?)\\|/g;' | rg -o '%[-a-z0-9]*' | sort -u )",
        -- Reload all unique tags
        ["alt-k"] = "reload(rg -NI '_\\|%.*\\|_' -- " .. fp .. "  | perl -ne 'print \"$1\n\" while /(%.*?)\\|/g;' |  sort -u)",
        -- Reload with all tags associated with selected root tag
        ["alt-s"] = "replace-query+reload(rg -Io '\\|%\\w+[^a-zA-Z%|]?[^a-zA-Z%|]?\\w+[^a-zA-Z%|]?[^a-zA-Z%|]?\\w+' -- " .. fp .. " | tr -d '|' | sort | uniq)",
        ["alt-q"] = "replace-query",
        -- Copy infile tag(s) to clipboard
        ["alt-y"] = "execute-silent(echo {+} | tr ' ' '|' | xargs -I tags copyq add tags)",
      }
    },
    fzf_opts = {
      ["--cycle"] = '',
      ["--multi"] = '',
   },
   actions = {
       ['default'] = function(selected)
      -- Build list of files selected
      local tag_list = ''
      for _, t in ipairs(selected) do
        tag_list = tag_list .. t .. ' '
      end
           local opts = {
               ["--query"] = tag_list,
               ["--exact"] = '',
               ["--cycle"] = '',
               ["--preview-window"] = 'nohidden:wrap',
           }
           fzf_lua.blines({fzf_opts = opts})
     end
   }
  })

end



-- %#% Open note under cursor
M.open_note = function (opts)

  opts = opts or {}
  opts.prompt           = "Popup> "
  opts.cwd              = "~/notes/notes"
  opts.previewer        = "builtin"

  vim.cmd('norm b')
  vim.fn.search("[0-9]\\{14}", "c")

  opts.fzf_opts = {
    ["--cycle"] = '',
    ["--query"] = vim.fn.expand("<cword>"),
  }

  opts.winopts = {
    preview = {
      layout = "vertical",
      vertical = "up:90%", wrap = "wrap"
    },
    width = 0.5 }

    opts.actions = { ["default"] = require'fzf-lua'.actions.file_edit }

  return fzf_lua.fzf_exec("ls ~/notes/notes/*.*md", opts)

end



-- %#% Menu with notes linked to in current note
M.list_links = function (opts)

  local fp = vim.api.nvim_buf_get_name(0)

  opts = opts or {}
  opts.prompt           = "Linked Notes> "
  opts.cwd              = "~/notes/notes"

  opts.fzf_opts = {
    ["--cycle"] = '',
    ["--with-nth"] = '1,2',
    ["--delimiter"] = '\\(',
    ["--preview"] = "echo {2} | sed 's/.$//' | xargs -I id bat --style=plain --color=always " .. os.getenv("HOME") .. "'/notes/notes/id.md'",
  }

  opts.winopts = {
    preview = {
      layout = "vertical",
      vertical = "up:85%", wrap = "wrap"
    },
    width = 0.8,
    height = 0.8,
  }

  opts.actions = {
    ["default"] = function(selected)
      -- Build list of files selected
      local f_list = ''
      for _, f in ipairs(selected) do

        for id in string.gmatch(f, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d') do
          local prefix = os.getenv('HOME') .. '/notes/notes/'
          f_list = f_list .. prefix .. id .. '.md' .. ' '
        end

      end
      local first_selected=''
      for f in string.gmatch(f_list, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d') do
          first_selected = '~/notes/notes/' .. f .. '.md'
          break
      end
      -- Add list of files to arglist and edit first selected
      vim.api.nvim_command(':$argadd ' .. f_list)
      vim.api.nvim_command(":buffer " .. first_selected)
    end,
  }

  return fzf_lua.fzf_exec("rg -oN '\\[.+\\]\\([0-9]{14}\\)' -- " .. fp, opts)

end



-- %#% Select opened notes from titles
M.select_opened_notes = function (opts)

  -- table of buffer references (even deleted buffers)
  local ls_output = vim.api.nvim_exec2('ls', {output = true})["output"]
  local patternToMatch = "%d%d%d%d%d%d%d%d%d%d%d%d%d%d"

  local fp =''

  for match in ls_output:gmatch(patternToMatch) do
       fp = fp .. "/home/hugo/notes/notes/" .. match .. ".md "
  end

  opts = opts or {}
  opts.prompt           = "Titles> "
  opts.cwd              = "~/notes/notes"

  opts.fzf_opts = {
    ["--cycle"] = '',
    ["--with-nth"] = '2',
    ["--delimiter"] = '~',
    ["--preview"] = "echo {1} | xargs -I id bat --style=plain --color=always id",
  }


  opts.winopts = {
    preview = {
      layout = "vertical",
      vertical = "up:70%", wrap = "wrap"
    },
    width = 0.8,
  }

   opts.actions = {
       ['default'] = function(selected)
         local path = string.match(selected[1], '%d+')
         vim.cmd('e ' .. '$HOME/notes/notes/' .. path .. '.md')
      end
   }

  return fzf_lua.fzf_exec('rg "^title:" ' .. fp .. ' | sed -e "s/:title:/ ~/"', opts)

end



-- %#% Select bookmarked files
M.open_bookmarks = function (opts)

  opts = opts or {}
  opts.prompt           = "Bookmarks> "
  opts.cwd              = "~/notes/notes"

  opts.fzf_opts = {
    ["--cycle"] = '',
    ["--with-nth"] = '1,2',
    ["--delimiter"] = '\\/',
    ["--preview"] = "echo {2} | xargs -I id bat --style=plain --color=always " .. os.getenv("HOME") .. "'/notes/notes/id.md'",
  }

  opts.winopts = {
    preview = {
      layout = "vertical",
      vertical = "up:70%", wrap = "wrap"
    },
    width = 0.8,
  }

  opts.actions = {
    ["default"] = function(selected)
      -- Build list of files selected
      local f_list = ''
      for _, f in ipairs(selected) do

        for id in string.gmatch(f, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d') do
          local prefix = os.getenv('HOME') .. '/notes/notes/'
          f_list = f_list .. prefix .. id .. '.md' .. ' '
        end

      end
      local first_selected=''
      for f in string.gmatch(f_list, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d') do
          first_selected = '~/notes/notes/' .. f .. '.md'
          break
      end
      -- Add list of files to arglist and edit first selected
      -- vim.api.nvim_command(':$argadd ' .. f_list)
      vim.api.nvim_exec2('$argadd ' .. f_list, {output = false})
      vim.api.nvim_exec2("buffer " .. first_selected, {output = false})
    end,
  }

  return fzf_lua.fzf_exec("cat ~/notes/notes/.bookmarks", opts)

end



-- %#% Insert image link
M.insert_image_link = function ()

  local path = vim.api.nvim_buf_get_name(0)
  local id = string.match(path, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d')
  local nvim_server = vim.api.nvim_exec2('echo v:servername', {output = true})["output"]
  vim.fn.system('$HOME/.scripts/notes.sh -I ' .. id .. ' ' .. nvim_server)

end



--[[ Utilities ]]--



-- %#% Go to next link
M.goto_next_link = function ()

  vim.api.nvim_exec2('vimgrep /](/g %', {output = false})

end



-- %#% Add header with current date below cursor
M.new_log_date = function ()

  local header = "### " .. os.date("%a, %d %b")
  local curspos = vim.api.nvim_win_get_cursor(0)

  vim.api.nvim_buf_set_lines(0, curspos[1] - 1, curspos[1], true, {
    '',
    header,
    '',
  })

end



-- %#% Template kukariri card
M.new_card = function (mode)

  local markup_start = '<!-- _||_ -->'
  local card_id = vim.fn.trim(vim.fn.system("date +%Y%m%d%H%M%S"))
  -- local today = vim.fn.trim(vim.fn.system("date -d $(date +%Y-%m-%d) +%s"))
  local interval = -1

  local markup_end = '----------'

  local curspos = vim.api.nvim_win_get_cursor(0)

  if mode == "default" then
    vim.api.nvim_buf_set_lines(0, curspos[1] - 1, curspos[1], true, {
      markup_start,
      '',
      'Card: ' .. card_id .. ' / NA / ' .. interval,
      '',
      '---',
      'title: Flash Card',
      '---',
      '',
      '<section id="front" style ="display: block;">',
      '',
      '## Front ',
      '',
      '* ',
      '',
      '<button id="toggleButton" type="button">Answer</button>',
      '',
      '</section>',
      '',
      '<section id="back" style="display: none;">',
      '',
      '## Back',
      '',
      '* ',
      '',
      '</section>',
      '',
      markup_end,
      '',
      '',
      '',
    })
  elseif mode == "audio" then
    vim.api.nvim_buf_set_lines(0, curspos[1] - 1, curspos[1], true, {
      markup_start,
      '',
      'Card: ' .. card_id .. ' / NA / ' .. interval,
      '',
      '---',
      'title: Espagnol',
      '---',
      '',
      '<section id="front" style ="display: block;">',
      '',
      '## Front ',
      '',
      '* ',
      '',
      '<button id="toggleButton" type="button">Answer</button>',
      '',
      '</section>',
      '',
      '<section id="back" style="display: none;">',
      '',
      '## Back',
      '',
      '* ',
      '',
      '<audio controls src="../audio/' .. card_id .. '.mp3" id="audioSample"></audio>',
      '',
      '</section>',
      '',
      markup_end,
      '',
      '',
      '',
    })
  end

end

return M


-- ----------------------------------------------------------------------------
--[[ Graveyard ]]--
--
-- %#% Live query of list of notes by matching patterns in 'tags' YAML field
-- M.tag_field_search = function (init, opts)
--
--    if (init > 0) then
--      local shell_cmd = "head -q -n4 *.md | sed  '/---$/d' | sd '^id: |^title: ' '' | paste -d';' - - - | rg -v '#:-old' > $HOME/notes/notes/.list-of-notes.txt"
--      vim.fn.jobstart(shell_cmd)
--      -- vim.fn.jobstart("rg --files-without-match '#:-old' ~/notes/notes/*.md > ~/notes/notes/.list-of-notes.txt")
--    end
--
--   opts = opts or {}
--   opts.prompt           = "Tags> "
--   opts.cwd              = "~/notes/notes"
--   opts.previewer        = "fzf-native"
--   opts.exec_empty_query = false
--
--   opts.fzf_opts = {
--     ["--cycle"] = '',
--     ["--tac"] = '',
--     ["--preview"] = vim.fn.shellescape("rg -NI {+} -- $HOME/notes/notes/.list-of-notes.txt | head -n1 | cut -f1 -d';' | xargs -I id bat --style=plain --color=always /home/hugo/notes/notes/id.md"),
--   }
--
--   opts.keymap = {
--     fzf = {
--       ["enter"]  = 'select-all+execute(echo {+} | tr " " "\n" > ~/notes/notes/.list-of-notes.txt)+deselect-all+beginning-of-line',
--       ["ctrl-x"] = 'clear-query',
--       ["alt-r"]  = "execute(rg --files-without-match '^tags.*-old+*' ~/notes/notes/*.md > ~/notes/notes/.list-of-notes.txt)+kill-line",
--           -- ":" in rg pattern creates bug so needed to use unnecessarily convoluted regex
--   }}
--
--   opts.actions = {
--     ["default"] = { function () end },
--     ['alt-m'] = fzf_lua.actions.file_edit,
--     ['alt-enter'] = { function(selected)
--       -- Build list of files selected
--       local f_list = ''
--       for _, f in ipairs(selected) do
--         f_list = f_list .. f .. ' '
--       end
--       -- Add list of files to arglist and edit first selected
--       vim.api.nvim_command(':$argadd ' .. f_list)
--       end,
--       fzf_lua.actions.resume
--     },
--     ['alt-t'] = { function() M.tag_list_search(0) end },
--   }

  -- return fzf_lua.fzf_exec("xargs -d '\n' -a $HOME/notes/notes/.list-of-notes.txt rg -IN '^tags: ' | sd " .. sd_pattern .. " | tr ',' '\n' | tr -d " .. tr_delete_pattern .. " | rg -IN '^#' | sort -u", opts)
  -- return fzf_lua.fzf_live(function(q)
    -- return "xargs -d '\n' -a ~/notes/notes/.list-of-notes.txt rg --no-messages -l -e 'tags:.*" .. vim.fn.shellescape(q or '') .. "' "
    -- local rg_pattern = '"' .. "^title:" .. '"'
    -- return "xargs -d '\n' -a ~/notes/notes/.list-of-notes.txt rg --no-messages -l -e 'tags:.*" .. vim.fn.shellescape(q or '') .. "' | xargs -I{} rg -m1 " .. rg_pattern .. ' {} | sd "title:" " ~"'
    -- return "xargs -d '\n' -a ~/notes/notes/.list-of-notes.txt rg --no-messages -l -e 'tags:.*" .. vim.fn.shellescape(q or '') .. "' | paste -d '~' - <(xargs -d '\n' -a ~/notes/notes/.list-of-notes.txt rg -m1 --no-messages -l -e '^title:')"
    -- return fzf_lua.fzf_exec("rg --no-messages -l -e 'tags:.*" .. query .. "' $HOME/notes/notes/.list-of-notes.txt", opts)
    -- local query = vim.fn.shellescape(q or '')
    -- local result = {}
    -- if q == '' or q == nil then
    --   local command = "cat /home/hugo/notes/notes/.list-of-notes.txt | cut -f2 -d';'"
    --   local output = io.popen(command)
    --   table.insert(result, output:read("a*"))
      -- for line in output:lines() do
      --   table.insert(result, line)
      -- end
      -- os.execute("echo " .. table.concat(result, ' ') .. " > ~/test.txt")
    -- else
      -- local command = "2>/dev/null rg -NI -e 'tags:.*" .. query .. "' $HOME/notes/notes/.list-of-notes.txt | cut -f2 -d';'"
      -- local output = io.popen(command)
      -- for line in output:lines() do
      --   table.insert(result, line)
      -- end
      -- table.insert(result, query)
    -- end
    -- return 'printf "' .. table.concat(result, "\n") .. '"'
    -- return 
    -- local command1 = "rg --no-messages -l -e 'tags:.*" .. query .. "'"
    -- local filepaths = {}
    -- for line in output1:lines() do
    --   table.insert(filepaths, line)
    -- end
    -- output1:close()

    -- local command2 = "paste -d '~' - <(printf " .. '"' .. filepaths .. '"' .. ")"
    -- local filepaths = "/home/hugo/notes/notes/20231217173405.md /home/hugo/notes/notes/20231217154823.md"
    -- local command2 = 'rg -N "title:" ' .. table.concat(filepaths, " ")

     -- local command2 = 'printf "' .. filepaths .. '"'
    -- local result = {}
    -- local output2 = io.popen(command2)
    -- for line in output2:lines() do
    --   table.insert(result, line)
    -- end
    -- output2:close()
    -- formatted_result = "default value"
     -- os.execute("echo " .. formatted_result .. " > ~/test.txt")
    -- return 'printf "' .. table.concat(result, "\n") .. '"'
    -- return 'printf "' .. table.concat(result, " ") .. '"'

--   end, opts)
--
-- end

-- M.select_opened_notes = function (opts)
--
--   -- table of buffer references (even deleted buffers)
--   local t = vim.api.nvim_list_bufs()
--
--   local fp = ''
--   for _, v in ipairs(t) do
--     -- get absolute filepath from buffer reference
--     local path = vim.api.nvim_buf_get_name(v)
--     -- extract only "notes" buffers
--     local match_index = string.find(path, "%d%d%d%d%d%d%d%d%d%d%d%d%d%d")
--     if match_index ~= nil then
--       fp = fp ..  vim.api.nvim_buf_get_name(v) .. ' '
--     end
--   end
--   print(fp)
--
--   opts = opts or {}
--   opts.prompt           = "Titles> "
--   opts.cwd              = "~/notes/notes"
--
--   opts.fzf_opts = {
--     ["--cycle"] = '',
--     ["--with-nth"] = '2',
--     ["--delimiter"] = '~',
--     ["--preview"] = vim.fn.shellescape("echo {1} | xargs -I id bat --style=plain --color=always id"),
--   }
--
--   opts.winopts = {
--     preview = {
--       layout = "vertical",
--       vertical = "up:70%", wrap = "wrap"
--     },
--     width = 0.8,
--   }
--
--    opts.actions = {
--        ['default'] = function(selected)
--          local path = string.match(selected[1], '%d+')
--          vim.cmd('e ' .. '$HOME/notes/notes/' .. path .. '.md')
--       end
--    }
--
--   return fzf_lua.fzf_exec('rg "^title:" ' .. fp .. ' | sed -e "s/:title:/ ~/"', opts)
--
-- end

-- M.new_note_and_link = function()
--
--   -- local fp = vim.api.nvim_buf_get_name(0)
--   -- local origin_id = string.match(fp, '%d%d%d%d%d%d%d%d%d%d%d%d%d%d')
--   local nvim_server = vim.api.nvim_exec2('echo v:servername', {output = true})["output"]
--   local id = vim.fn.trim(vim.fn.system("date +%Y%m%d%H%M%S"))
--   local tmux_command = 'tmux new-window -a -n new-note $HOME/.scripts/notes.sh -N ' .. id .. ' ' .. nvim_server
--   vim.fn.system(tmux_command)
--
--   -- local title = vim.fn.trim(vim.fn.system("$HOME/.scripts/notes.sh -t " .. id))
--
--   -- Insert link to new note at cursor position
--   -- vim.api.nvim_put({"[" .. title .. "](" .. id .. ")"}, "c", true, true)
--
--   -- Get ID of last created note
--   -- ls $HOME/notes/notes/*.md | tail -n 1 | rg -o "[0-9]{14}"
--   -- local id = vim.fn.trim(vim.fn.system([[
--   --     ls $HOME/notes/notes/*.md | \
--   --     tail -n 1 | \
--   --     rg -o '\d\d\d\d\d\d\d\d\d\d\d\d\d\d']]))
--
-- end
-- M.tag_list_search = function (opts)
-- 
--   opts = opts or {}
--   opts.prompt           = "Tags> "
--   opts.cwd              = "~/notes/notes"
-- 
--   opts.fzf_opts = {
--     ["--cycle"] = '',
--     ["--tac"] = '',
--   }
-- 
--   opts.keymap = {
--     fzf = {
--       -- ["enter"]  = "execute(xargs -d '\n' -a " .. os.getenv("HOME") .. "/notes/notes/.list-of-notes.txt rg --no-messages -l '^tags.*{}' > $HOME/notes/notes/.list-of-notes.txt)+reload(xargs -d '\n' -a $HOME/notes/notes/.list-of-notes.txt rg -IN '^tags' | perl -ne 'print \"$1\n\" while /(#[a-z].*?)\\x27/g;' | sort -u)",
--       ["ctrl-x"] = 'clear-query',
--       ["ctrl-q"] = 'replace-query',
--       ["alt-r"]  = "execute(rg --files-without-match '#:-old' ~/notes/notes/*.md > ~/notes/notes/.list-of-notes.txt)+reload(xargs -d '\n' -a $HOME/notes/notes/.list-of-notes.txt rg -IN '^tags' | perl -ne 'print \"$1\n\" while /(#[a-z].*?)\\x27/g;' | sort -u)",
--       -- Copy YAML tags to clipboard
--       -- ["alt-y"] = "execute@echo {+} | sed -En 's/([a-zA-Z0-9:#]+)/\x27\1,\x27/gp' | sed 's/.$//' | xargs -I tags copyq copy tags@",
--       -- ["alt-y"] = "execute-silent@echo {+} | perl -ne 'print \"\x27$1\n\" while /(#.*?)\x27/g;' | xargs -I tags copyq copy tags@",
--       ["alt-y"] = 'execute-silent@echo {+} | sed -e "s/ /-, /g" -e "s/#/-#/g" -e "s/$/-/" | sed "s/-/o/g"  | xargs -I tags copyq copy tags@',
--   }}
-- 
--   opts.actions = {
--     ['alt-m'] = fzf_lua.actions.file_edit,
--     ['alt-enter'] = function() M.tag_field_search(0) end,
--   }
-- 
--   -- return fzf_lua.fzf_exec("xargs -d '\n' -a $HOME/notes/notes/.list-of-notes.txt rg -IN '^tags: ' | perl -ne 'print \"$1\n\" while /(#[a-z].*?)\\x27/g;' | sort -u", opts)
--   -- local sd_pattern = '"' .. "^tags: \\['" .. '"' .. " '' "
--   -- local tr_delete_pattern = '"' .. "' ]" .. '"'
--   return fzf_lua.fzf_exec("$HOME/.scripts/notes.sh -l")
--   -- return fzf_lua.fzf_exec("xargs -d '\n' -a $HOME/notes/notes/.list-of-notes.txt rg -IN '^tags: ' | sd " .. sd_pattern .. " | tr ',' '\n' | tr -d " .. tr_delete_pattern .. " | sort -u", opts)
-- 
-- end
