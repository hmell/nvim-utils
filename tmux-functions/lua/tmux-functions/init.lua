local M = {}

-- %#% open lazygit for current repository
M.open_lazygit = function()
  local repo = vim.fn['expand']('%:p:h')
  local tmux_command = 'tmux new-window -a -c ' .. repo .. ' -n git lazygit'
  os.execute(tmux_command)
end

-- %#% open directory of current buffer
M.open_lf = function()
  local dir = vim.fn['expand']('%:p:h')
  local tmux_command = 'tmux splitw -c ' .. dir .. ' lf'
  os.execute(tmux_command)
end

-- %#% archive
-- M.knitter = function()
--   local rmd = vim.api.nvim_buf_get_name(0)
--   vim.cmd('w')
--   local tmux_command = 'tmux splitw -dI -l 35% knitter ' .. rmd
--   os.execute(tmux_command)
-- end

-- %#% Create and open supplementary folder for current note
M.new_supp_note = function(mode)

  local filename = vim.api.nvim_cmd({cmd = 'echo', args = {'expand("%:t:r")'}}, {output = true})
  local dir = os.getenv("HOME") .. '/notes/supp/' .. filename
  if mode == 'new' then
    vim.system({'mkdir', dir}):wait()
  end
  local tmux_command = 'tmux splitw -h -l 35% -c ' .. dir .. ' lf'
  os.execute(tmux_command)

end

-- %#% Create new formats/ subfolder in supp/ folder
M.new_format_note = function(output)

  local filename = vim.api.nvim_cmd({cmd = 'echo', args = {'expand("%:t:r")'}}, {output = true})
  local dir = '/home/hugo/notes/supp/' .. filename .. '/formats/' .. output
  vim.system({'mkdir', '-p', dir}):wait()
  local tmux_command = 'tmux splitw -l 35% -c ' .. dir .. ' lf'
  os.execute(tmux_command)
end

-- %#% Generate preview of quarto version of current note
M.quarto_preview = function(output, mode)

  local filename = vim.api.nvim_cmd({cmd = 'echo', args = {'expand("%:t:r")'}}, {output = true})
  local md = vim.api.nvim_buf_get_name(0)

  local dir = '/home/hugo/notes/supp/' .. filename .. '/formats/' .. output
  local qmd = dir .. '/index.qmd'

  if output == 'revealjs' then
    output = 'html'
  end

  print("hello")

  if mode == 'compile' then
    -- copy md file to qmd
    vim.system({'cp', md, qmd}):wait()
    -- render qmd to specified output and open document
    -- vim.system({'tmux','splitw','-dI', '-l', 'quarto', 'render', qmd, '--to', output}):wait()
    local tmux_command = 'tmux splitw -dI -l 35% quarto render ' .. qmd .. ' --to ' .. output
    os.execute(tmux_command)
  elseif mode == 'open' then
    -- open formatted version
    vim.system({'xdg-open', output_doc})
  end

end

-- %#% Open tmux split window for snippets
-- by default open nap tui
-- with "copy" as an argument -> run fnap to copy snippet to clipboard
M.nap = function(mode)

  -- local tmux_command = 'tmux splitw -l 35% -c ' .. dir .. ' lf'
  local tmux_command = ''
  if mode == 'copy' then
    tmux_command = 'tmux display-popup -E "nap list | gum filter | xargs nap | copyq copy -"'
  elseif mode == 'read' then
    tmux_command = 'tmux new-window -a -n snippets "nap list | fzf | xargs -I{} nap \"{}\" | gum format | gum pager --show-line-numbers=false --border=hidden"'
  else
    tmux_command = 'tmux new-window -a -n snippets nap'
  end
  os.execute(tmux_command)

end

return M
