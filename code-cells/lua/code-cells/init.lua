--[[

Code cells parsing, navigation and execution in notebooks and scripts.

--]]


local M = {}

local markdown_code_block = [[
  (
  fenced_code_block
  (info_string (language) @language)
  (code_fence_content) @code_content
  (fenced_code_block_delimiter) @delimiter
  ) @code_block
]]

local get_root = function(bufnr)
  local parser = vim.treesitter.get_parser(bufnr, "markdown", {})
  local tree = parser:parse()[1]
  return tree:root()
end

--- Module configuration.
M.config = {}

--- Set up the module.
M.setup = function(args)
  M.config = vim.tbl_deep_extend("force", M.config, args or {})
end

-- Get content and range for each code block
local parse_code_blocks = function (bufnr)

  bufnr = bufnr or vim.api.nvim_get_current_buf()

  -- Capture code block nodes
  local root = get_root(bufnr)
  local markdown_query = vim.treesitter.query.parse("markdown", markdown_code_block)

  -- Iterate over captured nodes and return info for each code block
  local results = {}
  for id, node in markdown_query:iter_captures(root, bufnr, 0, -1) do
    local name = markdown_query.captures[id]
    if name == "code_content" then
      local block = {}
      local range = { node:range() }
      block["row_start"] = range[1]
      block["row_end"] = range[3]
      block["content"] = vim.treesitter.get_node_text(node, bufnr)
      table.insert(results, block)
    end
  end

  return results

end

-- Parse cells in a script
local parse_cells_script = function (bufnr)

  -- Info on current buffer
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  local fp = vim.api.nvim_buf_get_name(bufnr)
  local nblines = vim.api.nvim_buf_line_count(bufnr)

  -- Get cell headers with ripgrep
  local cell_headers = vim.split(vim.fn.system('rg -n -e "%#%" ' .. fp), '\n')
  table.remove(cell_headers, #cell_headers)

  -- Extract range of cells
  local cells = {}
    -- Get start row of code cell
  for _, v in ipairs(cell_headers) do
    local cell = {
      row_start = tonumber(string.match(v, "^%d+")),
    }
    table.insert(cells, cell)
  end
    -- Get end row of code cell
  for i, v in ipairs(cells) do
    if i < #cells then
      v["row_end"] = cells[i+1]["row_start"] - 1
    else
      v["row_end"] = nblines
    end
  end

  return cells

end

-- Extract individual cells from a code block content
local split_into_cells = function (lines, start_index)

  local cells = {}
  local cell_index = 0
  local current_cell = {}

  for i,v in ipairs(lines) do
    print(string.find(v, "%%%#%%"))
    if string.match(v, "%%%#%%") ~= nil then
      cell_index = cell_index + 1
      if cell_index > 1 then
        table.insert(
          cells, {
            content = table.concat(current_cell, '\n'),
            row_start = start_index + i,
            row_end = start_index + i + #current_cell
          })
        current_cell = {v}
      else
        current_cell = {v}
      end
    elseif i == #lines and cell_index ~= 0 then
      table.insert(current_cell, v)
        table.insert(
          cells, {
            content = table.concat(current_cell, '\n'),
            row_start = start_index + i - (#current_cell - 1),
            row_end = start_index + i
          })
    else
      table.insert(current_cell, v)
    end
  end

  return cells

end

-- Create table with individual code cells from a list of code blocks
-- fields: content, row_start, row_end
local parse_code_cells = function (code)

  local code_cells = {}
  for _, block in ipairs(code) do
    local lines = vim.split(block["content"], "\n")
    -- Get individual code cells for current code block
    local cells = split_into_cells(lines, block["row_start"])
    for _, v in ipairs(cells) do
       table.insert(code_cells, v)
    end
  end

  return code_cells

end

-- Parse buffer to extract code cells
local parse_code = function (bufnr)

  local code_markdown = parse_code_blocks(bufnr)
  local code_cells = parse_code_cells(code_markdown)

  return code_cells

end

-- Send a range of lines for execution with 'slime'
local run_code = function (row_start, row_end)

     local slime_cmd = row_start .. ',' .. row_end .. 'SlimeSend'
     vim.cmd(slime_cmd)

end

-- Send current code cell for execution 
local execute_cell = function (cells)

  local winr = vim.api.nvim_get_current_win()
  local curpos = vim.api.nvim_win_get_cursor(winr)[1]

  -- Find current cell and run code
  for _, v in ipairs(cells) do
    if curpos >= v["row_start"] and curpos <= v["row_end"] then
      run_code(v["row_start"], v["row_end"])
      return
    end
  end

  print("Move the cursor to a code cell!")
  return 1

end

-- Move cursor to the next code cell
M.move_next_cell = function (source, bufnr, cells)

  source = source or "default"
  bufnr = bufnr or vim.api.nvim_get_current_buf()

  if source == "default" then
    if vim.bo[bufnr].filetype ~= "markdown" and vim.bo[bufnr].filetype ~= "quarto" then
      vim.notify("Only Markdown and Quarto are supported")
      return 1
    end
    cells = cells or parse_code(bufnr)
  elseif source == "script" then
    cells = cells or parse_cells_script(bufnr)
  end

  local winr = vim.api.nvim_get_current_win()
  local curpos = vim.api.nvim_win_get_cursor(winr)[1]

  for _, v in ipairs(cells) do
    if curpos < v["row_start"] then
      vim.api.nvim_win_set_cursor(winr, {v["row_start"], 1})
      return
    end
  end

end

-- Move cursor to the previous code cell
M.move_previous_cell = function (source, bufnr, cells)

  source = source or "default"
  bufnr = bufnr or vim.api.nvim_get_current_buf()

  if source == "default" then
    if vim.bo[bufnr].filetype ~= "markdown" and vim.bo[bufnr].filetype ~= "rmd" then
      vim.notify("Only Markdown and R Markdown are supported")
      return 1
    end
    cells = cells or parse_code(bufnr)
  elseif source == "script" then
    cells = cells or parse_cells_script(bufnr)
  end

  local winr = vim.api.nvim_get_current_win()
  local curpos = vim.api.nvim_win_get_cursor(winr)[1]

  for i = #cells, 1, -1 do
      local v = cells[i]
      if v["row_end"] < curpos then
        vim.api.nvim_win_set_cursor(winr, {v["row_start"], 1})
        return
      end
  end

end

-- Execute current cell code
M.run_cell = function (bufnr)

  bufnr = bufnr or vim.api.nvim_get_current_buf()
  if vim.bo[bufnr].filetype ~= "markdown" and vim.bo[bufnr].filetype ~= "rmd" then
    vim.notify("Only Markdown and R Markdown are supported")
    return 1

   end

  local buffer_cells = parse_code(bufnr)

  if #buffer_cells == 0 then print("No cell found") ; return end
  execute_cell(buffer_cells)

end

-- Execute current cell code and move to the next one
M.run_cell_move_next = function (bufnr)

  bufnr = bufnr or vim.api.nvim_get_current_buf()
  if vim.bo[bufnr].filetype ~= "markdown" and vim.bo[bufnr].filetype ~= "rmd" then
    vim.notify("Only Markdown and R Markdown are supported")
    return 1
   end

  local buffer_cells = parse_code(bufnr)
  local status = execute_cell(buffer_cells)
  if status ~= 1 then M.move_next_cell("default", bufnr, buffer_cells) end

end


M.run_cell_script = function (bufnr)

  bufnr = bufnr or vim.api.nvim_get_current_buf()
  local parsed_cells = parse_cells_script(bufnr)
  execute_cell(parsed_cells)

end


M.run_cell_move_next_script = function (bufnr)

  bufnr = bufnr or vim.api.nvim_get_current_buf()

  local buffer_cells = parse_cells_script(bufnr)
  local status = execute_cell(buffer_cells)
  if status ~= 1 then M.move_next_cell("script", bufnr, buffer_cells) end

end


-- Run all code cells
M.run_all = function(bufnr)
  local blocks = parse_code(bufnr)
  for _, v in ipairs(blocks) do
    run_code(v["row_start"], v["row_end"])
  end
end


M.run_all_script = function(bufnr)

  bufnr = bufnr or vim.api.nvim_get_current_buf()
  local buffer_cells = parse_cells_script(bufnr)

  for _, v in ipairs(buffer_cells) do
    run_code(v["row_start"], v["row_end"])
  end

end


return M
